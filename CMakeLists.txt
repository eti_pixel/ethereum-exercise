cmake_minimum_required (VERSION 3.0)
project (ethereum)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# Uncomment for including debug symbols
#add_compile_options("-g")

file(GLOB main_src
	"main/Controller.h"
	"main/Controller.cpp"
	"main/types.h"
	"main/**/*.h"
	"main/**/*.cpp")

file(GLOB test_src
	"test/*.h"
	"test/*.cpp"
	"test/**/*.h"
	"test/**/*.cpp")

add_executable(evaluate main/main.cpp ${main_src})

# Google Test

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

add_subdirectory(
	${CMAKE_CURRENT_BINARY_DIR}/vendor/googletest
	EXCLUDE_FROM_ALL)

add_executable(evaluate.test ${main_src} ${test_src})
target_link_libraries(evaluate.test gtest_main)
add_test(NAME TestSuite COMMAND evaluate.test)

add_custom_command(
	TARGET evaluate.test
	POST_BUILD
	COMMAND evaluate.test)