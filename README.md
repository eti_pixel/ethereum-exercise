Parser task
===========


## ![](https://raw.githubusercontent.com/google/material-design-icons/master/file/1x_web/ic_file_download_black_18dp.png) Obtaining the sources

This repository includes Google Test as a submodule. Clone it as follows:
```
git clone --recurse-submodules https://eti_pixel@bitbucket.org/eti_pixel/ethereum-exercise.git
```

If the repository was cloned without submodules, the submodule can be loaded as follows:
```
git submodule init
git submodule update
```

Alternatively, you can download a tagged version (does not require a Git client):

https://bitbucket.org/eti_pixel/ethereum-exercise/downloads/?tab=tags

## ![](https://raw.githubusercontent.com/google/material-design-icons/master/action/1x_web/ic_build_black_18dp.png) Building

_The application comes with pre-built x86-64 binaries for Linux, Mac and Windows._

The application can be built **locally** or using **Docker and Docker Compose**.

### Local

Building requires [CMake](https://cmake.org/) and a C++11-compatible compiler. Compilation was tested with Clang (Linux & Mac) and GCC (Linux).

In the project directory:

```
cmake . && make
```

The built binaries can be found in the `bin` directory.

_The building process executes the unit tests and fails if the tests are not successful. This can be suppressed by executing `make evaluate` instead._

### Docker container

Building requires [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).

In the project directory:
```
docker-compose build
```

This will result in a container named `ethereum_exercise`.

_The building process executes the unit tests and fails if the tests are not successful. This can be suppressed by removing the `add_custom_command` directive at the end of CMakeLists.txt._


## ![](https://raw.githubusercontent.com/google/material-design-icons/master/maps/1x_web/ic_directions_run_black_18dp.png) Running

The application reads an expression from the standard input. It can therefore be started interactively, or an expression can be piped to it.

### Using precompiled binaries

Depending on your OS:
```
dist/linux/evaluate
dist/mac/evaluate
dist\win\evaluate.exe
```

_Note: the binaries are compiled for x86-64 environments._

### Local

In the project directory:
```
bin/evaluate
echo '(1-1)*(2+3+4+5+6+7+8)+9*(1-1)' | ./evaluate
```

### Docker container

Running interactively with the wrapper script:
```
./evaluate.sh
echo '(1-1)*(2+3+4+5+6+7+8)+9*(1-1)' | ./evaluate.sh
```

Running interactively without the wrapper script:
```
docker run -i ethereum_exercise
echo '(1-1)*(2+3+4+5+6+7+8)+9*(1-1)' | docker run -i ethereum_exercise
```


## ![](https://raw.githubusercontent.com/google/material-design-icons/master/action/1x_web/ic_done_all_black_18dp.png) Testing

This application uses Google Test as its unit testing platform. The tests are defined in [test/test.cpp](https://bitbucket.org/eti_pixel/ethereum-exercise/src/master/test/test.cpp).

By default, the tests are executed at build time.

With the precompiled binaries, they can be also be executed as follows (depending on your OS):
```
dist/<os>/evaluate.test
```

With local builds, the tests can be executed as follows:
```
bin/evaluate.test
```


## ![](https://raw.githubusercontent.com/google/material-design-icons/master/image/1x_web/ic_edit_black_18dp.png) Software design

### File structure

-	`bin` - locally compiled binaries
-	`dist` - precompiled binaries
-	`main` - the main source files
	-	`api` - interfaces
	-	`evaluator` - the `Evaluator` and the visitor
	-	`exception` - some exception classes
	-	`parser` - the `Parser` and the visitor
	-	`structure` - the abstract syntax tree implementation
-	`test` - the test source files
-	`vendor` - 3rd party dependencies, contains Google Test


### Design

The code can be broken down into four major parts:

-	The abstract syntax tree (`SyntaxTree`), implemented as a tree of nodes (`TreeNode`). The `SyntaxTree` itself is nothing more than a `TreeNode` without parent.
-	The parser (`Parser`) breaks down the input string into `TreeNode`s and uses them to build the AST.
-	The evaluator (`Evaluator`) traverses a previously built AST and returns the result.
-	The controller (`Controller`) holds the tree, reads the input and invokes the parser and the evaluator.

The AST, parser and evaluator implement a **visitor pattern**, to separate the different algorithms from the structure.

The `Parser` first builds a single-level tree, then uses an `OperatorVisitor` to break down the levels according to the priority of operators. In this process, most of the input validations take place. The operations on the tree happen in-place.

The `Evaluator` uses an `EvaluatingVisitor` to traverse the tree. The visitor first navigates from the root towards the leaves, then navigates back up, evaluating each leaf, operator or group.


## ![](https://raw.githubusercontent.com/google/material-design-icons/master/action/1x_web/ic_trending_up_black_18dp.png) Possible improvements & limitations

For the sake of simplicity, and according to the task description, the application has a number of limitations:

-	There is no sophisticated tokenizer.
-	The operators are limited to +, -, *, and / and are all binary operators.
-	The application is monolithic and single threaded; for much bigger applications in productive scenarios, one should consider designing the application in a distributed and service-oriented way. In such situations, security aspects should be taken into consideration, of course.
-	Very long expressions could theoretically lead to a stack overflow, since the tree traversal algorithms (visitors) are invoking methods recursively.
-	Performance could be improved by using maps in the `TreeNode` implementation, to avoid a linear complexity for operations like `removeChild()` and `getIndexInLevel()`.