#include "OperatorVisitor.h"

#pragma GCC diagnostic ignored "-Wformat"
#pragma clang diagnostic ignored "-Wformat"

using namespace std;
using namespace eth;

OperatorVisitor::OperatorVisitor()
{
}

OperatorVisitor::~OperatorVisitor()
{
}

ValueType OperatorVisitor::visit(const SyntaxTree & tree)
{
	this->visitChildren(tree.getChildren());
	return 0;
}

ValueType OperatorVisitor::visit(const Group & group)
{
	this->visitChildren(group.getChildren());
	return 0;
}

ValueType OperatorVisitor::visit(const BinaryOperator & op)
{
	this->visitChildren(op.getChildren());
	return 0;
}

ValueType OperatorVisitor::visit(const Literal & literal)
{
	return 0;
}

void OperatorVisitor::visitChildren(const ChildContainer & children)
{
	shared_ptr<BinaryOperator> candidateOperator = nullptr;
	shared_ptr<BinaryOperator> currentOperator;

	for (size_t i = 0; i < children.size(); ++i)
	{
		if (instanceOf<BinaryOperator>(children[i].get()))
		{
			currentOperator = static_pointer_cast<BinaryOperator>(children[i]);

			// Delegate the operator with the lowest priority;
			// if there are multiple operators with the same priority, pick the last one
			if (!candidateOperator || !this->higherPrecedence(currentOperator, candidateOperator))
				candidateOperator = shared_ptr<BinaryOperator>(currentOperator);
		}
	}

	if (candidateOperator)
		this->visitChildrenWithOperator(candidateOperator, children);
	else
		this->visitChildrenWithoutOperator(children);
}

void OperatorVisitor::visitChildrenWithOperator(
	shared_ptr<BinaryOperator> candidateOperator,
	const ChildContainer & children)
{
	vector<shared_ptr<TreeNode>> left;
	vector<shared_ptr<TreeNode>> right;

	Utility::split(children, *candidateOperator, left, right);

	if (left.empty() || right.empty())
		throw InputException("Missing operands for operator");

	this->siblingsToChildrenGroup(candidateOperator, left);
	this->siblingsToChildrenGroup(candidateOperator, right);

	candidateOperator->accept(*this);
}

void OperatorVisitor::visitChildrenWithoutOperator(const ChildContainer & children)
{
	// List of children without operator:
	// The only circumstance in which it is allowed to have a level without
	// operators is when there is only one or two children (group or leaf;
	// we assume the detection loop would've detected the operator).

	// No children, this is okay
	if (children.empty())
		return;

	// 3 or more children, but no operator could be found
	if (children.size() > 2)
		throw InputException("Missing operator");

	// Parent is a group but with more than one child
	if (instanceOf<Group>(children[0]->getParent()) && children.size() > 1)
		throw InputException("Missing operator");

	// Parent is an operator but with more than two children
	if (instanceOf<BinaryOperator>(children[0]->getParent()) && children.size() > 2)
		throw InputException("Missing operator");

	// Parent is an operator but with less than two children
	if (instanceOf<BinaryOperator>(children[0]->getParent()) && children.size() < 2)
		throw InputException("Missing operands for operator");

	for (size_t i = 0; i < children.size(); ++i)
		children[i]->accept(*this);
}

void OperatorVisitor::siblingsToChildrenGroup(shared_ptr<TreeNode> node, ChildContainer siblings)
{
	// Remove the node's siblings from their parent, add them to a group,
	// and add the group as a child of the node.

	shared_ptr<Group> group;

	if (siblings.size() == 1)
	{
		// Only one child, no need to create a group
		node->getParent()->removeChild(siblings[0]);
		node->addChild(siblings[0]);
		return;
	}

	group = make_shared<Group>();

	for (size_t i = 0; i < siblings.size(); ++i)
	{
		node->getParent()->removeChild(siblings[i]);
		group->addChild(siblings[i]);
	}

	node->addChild(group);
}

bool OperatorVisitor::higherPrecedence(const shared_ptr<BinaryOperator> & op,
	const shared_ptr<BinaryOperator> & other) const
{
	return (op->getCode() == BinaryOperator::MULTIPLICATION ||
		op->getCode() == BinaryOperator::DIVISION) &&
		(other->getCode() == BinaryOperator::ADDITION ||
		other->getCode() == BinaryOperator::SUBTRACTION);
}

bool OperatorVisitor::lowerPrecedence(const shared_ptr<BinaryOperator> & op,
	const shared_ptr<BinaryOperator> & other) const
{
	return (op->getCode() == BinaryOperator::ADDITION ||
		op->getCode() == BinaryOperator::SUBTRACTION) &&
		(other->getCode() == BinaryOperator::MULTIPLICATION ||
		other->getCode() == BinaryOperator::DIVISION);
}