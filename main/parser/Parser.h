#ifndef _PARSER_H_
#define _PARSER_H_

#include <memory>
#include <stack>
#include <string>

#include "../structure/SyntaxTree.h"
#include "../structure/Group.h"
#include "../structure/BinaryOperator.h"
#include "../structure/Literal.h"
#include "../exception/InputException.h"
#include "OperatorVisitor.h"

namespace eth
{
	typedef char Token;

	class Parser
	{
	public:
		Parser(std::shared_ptr<SyntaxTree> tree);
		virtual ~Parser();

		void parse(const std::string & input);

	protected:
		static const Token LEFT_PARENTHESIS = '(';
		static const Token RIGHT_PARENTHESIS = ')';
		static const Token ASCII_0 = 0x30;
		static const Token ASCII_9 = 0x39;

		std::shared_ptr<SyntaxTree> tree;
		std::shared_ptr<TreeNode> currentNode;
		std::stack<std::shared_ptr<TreeNode>> nodeStack;

		virtual void parse(const Token & token);
		virtual bool canIgnore(const Token & token) const;
		virtual bool isNumeric(const Token & token) const;
	};
}

#endif