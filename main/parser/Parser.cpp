#include "Parser.h"

using namespace std;
using namespace eth;

Parser::Parser(shared_ptr<SyntaxTree> tree) :
	tree(tree),
	currentNode(tree)
{
}

Parser::~Parser()
{
}

void Parser::parse(const string & input)
{
	OperatorVisitor operatorVisitor;

	for (size_t i = 0; i < input.length(); ++i)
	{
		this->parse(input[i]);
	}

	if (this->tree->getChildCount() <= 0)
		throw InputException("Missing expression");

	operatorVisitor.visit(*this->tree);

	if (!this->nodeStack.empty())
		throw InputException("Missing right parenthesis");
}

void Parser::parse(const Token & token)
{
	if (this->canIgnore(token))
	{
	}
	else if (token == BinaryOperator::ADDITION ||
		token == BinaryOperator::SUBTRACTION ||
		token == BinaryOperator::MULTIPLICATION ||
		token == BinaryOperator::DIVISION)
	{
		this->currentNode->addChild(make_shared<BinaryOperator>(BinaryOperator(token)));
	}
	else if (token == Parser::LEFT_PARENTHESIS)
	{
		this->nodeStack.push(this->currentNode);
		this->nodeStack.top()->addChild(this->currentNode = make_shared<Group>());
	}
	else if (token == Parser::RIGHT_PARENTHESIS)
	{
		if (this->nodeStack.empty())
			throw InputException("Unexpected right parenthesis");

		this->currentNode = this->nodeStack.top();
		this->nodeStack.pop();
	}
	else if (this->isNumeric(token))
	{
		this->currentNode->addChild(make_shared<Literal>(Literal(token - Parser::ASCII_0)));
	}
	else
	{
		throw InputException("Invalid input");
	}
}

bool Parser::canIgnore(const Token & token) const
{
	return (token == ' ' || token == '\t' || token == '\n' || token == '\r');
}

bool Parser::isNumeric(const Token & token) const
{
	return token >= Parser::ASCII_0 && token <= Parser::ASCII_9;
}