#ifndef _OPERATOR_VISITOR_H_
#define _OPERATOR_VISITOR_H_

#include "../api/Visitor.h"
#include "../structure/SyntaxTree.h"
#include "../structure/Group.h"
#include "../structure/BinaryOperator.h"
#include "../structure/Literal.h"
#include "../structure/Utility.h"
#include "../exception/IllegalStateException.h"
#include "../exception/InputException.h"
#include "../exception/UnsupportedException.h"

namespace eth
{
	/**
	 * A visitor which breaks down tree levels to prioritize operators
	 * according to arithmetic precedence rules.
	 *
	 * The visitor analyzes the children of a visited node, selecting the
	 * lowests-priority operator and pushing down the other children down by a
	 * level.
	 */
	class OperatorVisitor : public Visitor
	{
	public:
		OperatorVisitor();
		virtual ~OperatorVisitor();

		virtual ValueType visit(const SyntaxTree & literal) override;
		virtual ValueType visit(const Group & group) override;
		virtual ValueType visit(const BinaryOperator & op) override;
		virtual ValueType visit(const Literal & literal) override;

	protected:
		virtual void visitChildren(const ChildContainer & children);
		virtual void visitChildrenWithOperator(
			std::shared_ptr<BinaryOperator> candidateOperator,
			const ChildContainer & children);
		virtual void visitChildrenWithoutOperator(
			const ChildContainer & children);

		virtual void siblingsToChildrenGroup(
			std::shared_ptr<TreeNode> node, ChildContainer siblings);

		virtual bool higherPrecedence(
			const std::shared_ptr<BinaryOperator> & op,
			const std::shared_ptr<BinaryOperator> & other) const;
		virtual bool lowerPrecedence(
			const std::shared_ptr<BinaryOperator> & op,
			const std::shared_ptr<BinaryOperator> & other) const;
	};
}

#endif