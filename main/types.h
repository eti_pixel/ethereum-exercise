#ifndef _TYPES_H_
#define _TYPES_H_

typedef float ValueType;

template<typename Base, typename T>
inline bool instanceOf(const T * object)
{
	return dynamic_cast<const Base *>(object);
}

#endif