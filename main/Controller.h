#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <iostream>
#include <memory>
#include "parser/Parser.h"
#include "evaluator/Evaluator.h"
#include "structure/SyntaxTree.h"

namespace eth
{
	class Controller
	{
	public:
		Controller();
		virtual ~Controller();

		void run();

	protected:
		std::shared_ptr<SyntaxTree> tree;
	};
}

#endif