#ifndef _VISITABLE_H_
#define _VISITABLE_H_

#include "../types.h"

namespace eth
{
	class Visitor;

	class Visitable
	{
	public:
		virtual ValueType accept(Visitor & visitor) const = 0;
	};
}

#endif