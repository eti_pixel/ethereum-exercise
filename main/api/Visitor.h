#ifndef _VISITOR_H_
#define _VISITOR_H_

#include "../types.h"

namespace eth
{
	class SyntaxTree;
	class Group;
	class BinaryOperator;
	class Literal;

	class Visitor
	{
	public:
		virtual ValueType visit(const SyntaxTree & tree) = 0;
		virtual ValueType visit(const Group & op) = 0;
		virtual ValueType visit(const BinaryOperator & op) = 0;
		virtual ValueType visit(const Literal & literal) = 0;
	};
}

#endif