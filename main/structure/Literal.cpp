#include "Literal.h"

using namespace std;
using namespace eth;

Literal::Literal(const ValueType & value) :
	value(value)
{
}

Literal::~Literal()
{
}

ValueType Literal::getValue() const
{
	return this->value;
}

ValueType Literal::accept(Visitor & visitor) const
{
	return visitor.visit(*this);
}

TreeNode * Literal::addChild(const std::shared_ptr<TreeNode> node)
{
	// Literals shouldn't have children
	throw UnsupportedException("Literals cannot have children");
}