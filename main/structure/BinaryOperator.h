#ifndef _OPERATOR_H_
#define _OPERATOR_H_

#include "TreeNode.h"
#include "../api/Visitor.h"
#include "../exception/IllegalStateException.h"
#include "../exception/UnsupportedException.h"

namespace eth
{
	class BinaryOperator : public TreeNode
	{
	public:
		static const char ADDITION = '+';
		static const char SUBTRACTION = '-';
		static const char MULTIPLICATION = '*';
		static const char DIVISION = '/';

		BinaryOperator(const char code);
		virtual ~BinaryOperator();

		virtual ValueType accept(Visitor & visitor) const override;

		virtual char getCode() const;
		virtual std::shared_ptr<TreeNode> getLeftOperand() const;
		virtual std::shared_ptr<TreeNode> getRightOperand() const;
		virtual void setLeftOperand(const std::shared_ptr<TreeNode> operand);
		virtual void setRightOperand(const std::shared_ptr<TreeNode> operand);
	protected:
		const char code;
		std::shared_ptr<TreeNode> leftOperand;
		std::shared_ptr<TreeNode> rightOperand;
	};
}

#endif