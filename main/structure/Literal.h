#ifndef _LITERAL_H_
#define _LITERAL_H_

#include "TreeNode.h"
#include "../api/Visitor.h"
#include "../exception/UnsupportedException.h"

namespace eth
{
	class Literal : public TreeNode
	{
	public:
		Literal(const ValueType & value);
		virtual ~Literal();

		virtual ValueType getValue() const;

		virtual ValueType accept(Visitor & visitor) const override;

		virtual TreeNode * addChild(const std::shared_ptr<TreeNode> node) override;

	private:
		const ValueType value;
	};
}

#endif