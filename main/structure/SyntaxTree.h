#ifndef _SYNTAX_TREE_H_
#define _SYNTAX_TREE_H_

#include "TreeNode.h"
#include "../api/Visitor.h"

namespace eth
{
	class SyntaxTree : public TreeNode
	{
	public:
		SyntaxTree();
		virtual ~SyntaxTree();

		virtual ValueType accept(Visitor & visitor) const override;
	};
}

#endif