#include "SyntaxTree.h"

using namespace eth;

SyntaxTree::SyntaxTree()
{
}

SyntaxTree::~SyntaxTree()
{
}

ValueType SyntaxTree::accept(Visitor & visitor) const
{
	return visitor.visit(*this);
}