#include "BinaryOperator.h"

using namespace std;
using namespace eth;

BinaryOperator::BinaryOperator(const char code) :
	code(code),
	leftOperand(nullptr),
	rightOperand(nullptr)
{
}

BinaryOperator::~BinaryOperator()
{
}

ValueType BinaryOperator::accept(Visitor & visitor) const
{
	return visitor.visit(*this);
}

char BinaryOperator::getCode() const
{
	return this->code;
}

shared_ptr<TreeNode> BinaryOperator::getLeftOperand() const
{
	if (this->getChildCount() < 1)
		throw IllegalStateException("The operator does not have a left operand");
	else if (this->getChildCount() > 2)
		throw IllegalStateException("The operator has more than two operands");

	return this->getChildren()[0];
}

shared_ptr<TreeNode> BinaryOperator::getRightOperand() const
{
	if (this->getChildCount() < 2)
		throw IllegalStateException("The operator does not have a right operand");
	else if (this->getChildCount() > 2)
		throw IllegalStateException("The operator has more than two operands");

	return this->getChildren()[1];
}

void BinaryOperator::setLeftOperand(const shared_ptr<TreeNode> operand)
{
	if (this->leftOperand.get())
		this->removeChild(this->leftOperand);

	this->leftOperand = operand;
	this->TreeNode::addChild(operand);
}

void BinaryOperator::setRightOperand(const shared_ptr<TreeNode> operand)
{
	if (this->rightOperand.get())
		this->removeChild(this->rightOperand);

	this->rightOperand = operand;
	this->TreeNode::addChild(operand);
}