#ifndef _GROUP_H_
#define _GROUP_H_

#include "TreeNode.h"
#include "../api/Visitor.h"

namespace eth
{
	/**
	 * A simple tree node used to group children.
	 */
	class Group : public TreeNode
	{
	public:
		Group();
		virtual ~Group();

		virtual ValueType accept(Visitor & visitor) const override;
	};
}

#endif