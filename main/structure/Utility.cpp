#include "Utility.h"

using namespace std;
using namespace eth;

void Utility::split(
	const ChildContainer & children, const TreeNode & node,
	ChildContainer & left, ChildContainer & right)
{
	bool foundNode = false;

	for (size_t i = 0; i < children.size(); ++i)
	{
		if (children[i].get() == &node)
			foundNode = true;
		else if (foundNode)
			right.push_back(children[i]);
		else
			left.push_back(children[i]);
	}
}

void Utility::dumpTree(const TreeNode & node)
{
	Utility::dumpTree(node, 0);
}

void Utility::dumpNode(const TreeNode & node)
{
	#if VERBOSE == 1

	if (instanceOf<Group>(&node))
	{
		cout << "[Group]";
		cout << " :: " << node.getChildCount() << " children" << endl;
	}
	else if (instanceOf<BinaryOperator>(&node))
	{
		cout << "[BinaryOperator " << dynamic_cast<const BinaryOperator *>(&node)->getCode() << "]";
		cout << " :: " << node.getChildCount() << " children" << endl;
	}
	else if (instanceOf<Literal>(&node))
	{
		cout << "[Literal " << dynamic_cast<const Literal *>(&node)->getValue() << "]";
		cout << " :: " << node.getChildCount() << " children" << endl;
	}
	else
	{
		cout << "[TreeNode?]";
		cout << " :: " << node.getChildCount() << " children" << endl;
	}

	#endif
}

void Utility::dumpTree(const TreeNode & node, size_t level)
{
	#if VERBOSE == 1

	for (size_t space = 0; space < level * 2; ++space)
		cout << '.';
	cout << "  ";
	Utility::dumpNode(node);

	for (size_t i = 0; i < node.getChildCount(); ++i)
		Utility::dumpTree(*node.getChildren()[i], level + 1);

	#endif
}