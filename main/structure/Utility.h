#ifndef _UTILITY_H_
#define _UTILITY_H_

#include <iostream>
#include "TreeNode.h"
#include "SyntaxTree.h"
#include "Group.h"
#include "BinaryOperator.h"
#include "Literal.h"
#include "../global.h"
#include "../types.h"

namespace eth
{
	class Utility
	{
	public:
		static void split(
			const ChildContainer & children, const TreeNode & node,
			ChildContainer & left, ChildContainer & right);

		static void dumpTree(const TreeNode & node);
		static void dumpNode(const TreeNode & node);

	protected:
		static void dumpTree(const TreeNode & node, size_t level);
	};
}

#endif