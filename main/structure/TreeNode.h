#ifndef _TREE_NODE_H_
#define _TREE_NODE_H_

#include <vector>
#include <memory>
#include "../api/Visitable.h"
#include "../exception/IllegalStateException.h"
#include "../exception/UnsupportedException.h"

namespace eth
{
	class TreeNode;
	typedef std::vector<std::shared_ptr<TreeNode>> ChildContainer;

	class TreeNode : public Visitable
	{
	public:
		TreeNode();
		virtual ~TreeNode();

		/**
		 * Add the node as a child of this node, and returns this node.
		 */
		virtual TreeNode * addChild(std::shared_ptr<TreeNode> node);

		/**
		 * Attempts to remove the node from the list of children of this node, and returns this node.
		 * This method silently returns if the child could not be found.
		 */
		virtual TreeNode * removeChild(std::shared_ptr<TreeNode> node);

		virtual void removeSiblings();
		virtual void siblingsToChildren();

		virtual TreeNode * getParent() const;
		virtual const ChildContainer & getChildren() const;
		virtual std::size_t getChildCount() const;
		virtual const ChildContainer getLeftSiblings() const;
		virtual const ChildContainer getRightSiblings() const;
		virtual bool isRoot() const;
		virtual bool isLeaf() const;

	protected:
		TreeNode * parent;
		ChildContainer children;

		std::size_t getChildIndex(const TreeNode * node) const;
		std::size_t getIndexInLevel() const;
	};
}

#endif