#include "TreeNode.h"

using namespace std;
using namespace eth;

TreeNode::TreeNode() :
	//parent(shared_ptr<TreeNode>(nullptr)),
	parent(nullptr),
	children()
{
}

TreeNode::~TreeNode()
{
}

TreeNode * TreeNode::addChild(shared_ptr<TreeNode> node)
{
	this->children.push_back(node);
	node->parent = this;

	return this;
}

TreeNode * TreeNode::removeChild(shared_ptr<TreeNode> node)
{
	shared_ptr<TreeNode> child;

	for (ChildContainer::iterator it = this->children.begin(); it != this->children.end(); ++it)
	{
		if (it->get() == node.get())
		{
			child = *it;
			child->parent = nullptr;
			this->children.erase(it);
			break;
		}
	}

	return this;
}

void TreeNode::siblingsToChildren()
{
	shared_ptr<TreeNode> sibling;

	if (this->isRoot())
		throw UnsupportedException("Cannot turn siblings into children on a root node");

	for (size_t i = 0; i < this->getParent()->getChildCount(); ++i)
	{
		sibling = this->getParent()->getChildren()[i];

		if (sibling.get() != this)
		{
			this->parent->removeChild(sibling);
			--i;
			this->addChild(sibling);
		}
	}
}

void TreeNode::removeSiblings()
{
	shared_ptr<TreeNode> sibling;

	if (this->isRoot())
		throw UnsupportedException("Cannot remove siblings from a root node");

	for (size_t i = 0; i < this->getParent()->getChildCount(); ++i)
	{
		sibling = this->getParent()->getChildren()[i];

		if (sibling.get() != this)
		{
			this->parent->removeChild(sibling);
			--i;
		}
	}
}

TreeNode * TreeNode::getParent() const
{
	if (this->isRoot())
		throw IllegalStateException("A root node has no parent");

	return this->parent;
}

const ChildContainer & TreeNode::getChildren() const
{
	return this->children;
}

size_t TreeNode::getChildCount() const
{
	return this->children.size();
}

const ChildContainer TreeNode::getLeftSiblings() const
{
	ChildContainer result;
	size_t index = this->getIndexInLevel();

	for (size_t i = 0; i < index; ++i)
		result.push_back(this->getParent()->getChildren()[i]);

	return result;
}

const ChildContainer TreeNode::getRightSiblings() const
{
	ChildContainer result;
	size_t index = this->getIndexInLevel();

	for (size_t i = index + 1; i < this->getParent()->getChildCount(); ++i)
		result.push_back(this->getParent()->getChildren()[i]);

	return result;
}

bool TreeNode::isRoot() const
{
	return !this->parent;
}

bool TreeNode::isLeaf() const
{
	return this->children.empty();
}

size_t TreeNode::getIndexInLevel() const
{
	if (this->isRoot())
		return 0;

	return this->getParent()->getChildIndex(this);
}

size_t TreeNode::getChildIndex(const TreeNode * node) const
{
	for (size_t i = 0; i < this->getChildCount(); ++i)
	{
		if (this->children[i].get() == node)
			return i;
	}

	throw IllegalStateException("Child not found");
}