#include "Controller.h"

using namespace std;
using namespace eth;

Controller::Controller() :
	tree(make_shared<SyntaxTree>())
{
}

Controller::~Controller()
{
}

void Controller::run()
{
	Parser parser = Parser(this->tree);
	Evaluator evaluator = Evaluator(this->tree);
	string input;
	ValueType output;

	cout << "Enter input:" << endl;
	getline(cin, input);

#if VERBOSE
	cout << "Expression: " << input << endl;
#endif

	parser.parse(input);
	output = evaluator.evaluate();

	cout << "Result: " << output << endl;
}