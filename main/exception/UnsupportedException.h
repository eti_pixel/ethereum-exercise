#ifndef _UNSUPPORTED_EXCEPTION_H_
#define _UNSUPPORTED_EXCEPTION_H_

#include "Exception.h"

namespace eth
{
	class UnsupportedException : public Exception
	{
	public:
		UnsupportedException();
		UnsupportedException(const std::string & message);
		UnsupportedException(const char * message);
		virtual ~UnsupportedException();
	};
}

#endif