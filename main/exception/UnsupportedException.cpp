#include "UnsupportedException.h"

using namespace eth;

UnsupportedException::UnsupportedException() :
	Exception("Operation not supported")
{
}

UnsupportedException::UnsupportedException(const std::string & message) :
	Exception(message)
{
}

UnsupportedException::UnsupportedException(const char * message) :
	Exception(message)
{
}

UnsupportedException::~UnsupportedException()
{
}