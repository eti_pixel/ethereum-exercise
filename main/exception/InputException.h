#ifndef _INPUT_EXCEPTION_H_
#define _INPUT_EXCEPTION_H_

#include "Exception.h"

namespace eth
{
	class InputException : public Exception
	{
	public:
		InputException();
		InputException(const std::string & message);
		InputException(const char * message);
		virtual ~InputException();
	};
}

#endif