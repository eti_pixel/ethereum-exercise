#include "Exception.h"

using namespace eth;

Exception::Exception() :
	message(nullptr)
{
}

Exception::Exception(const std::string & message) :
	message(message.c_str())
{
}

Exception::Exception(const char * message) :
	message(message)
{
}

Exception::~Exception()
{
}

char const * Exception::what() const throw()
{
	return this->message;
}