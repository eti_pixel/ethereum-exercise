#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

#include <exception>
#include <string>

namespace eth
{
	class Exception : public std::exception
	{
	public:
		Exception();
		Exception(const std::string & message);
		Exception(const char * message);
		virtual ~Exception();

		virtual const char * what() const throw() override;
	
	protected:
		const char * message;
	};
}

#endif