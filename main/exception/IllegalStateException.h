#ifndef _ILLEGAL_STATE_EXCEPTION_H_
#define _ILLEGAL_STATE_EXCEPTION_H_

#include "Exception.h"

namespace eth
{
	class IllegalStateException : public Exception
	{
	public:
		IllegalStateException();
		IllegalStateException(const std::string & message);
		IllegalStateException(const char * message);
		virtual ~IllegalStateException();
	};
}

#endif