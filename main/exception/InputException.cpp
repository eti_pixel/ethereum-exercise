#include "InputException.h"

using namespace eth;

InputException::InputException() :
	Exception("Invalid input")
{
}

InputException::InputException(const std::string & message) :
	Exception(message)
{
}

InputException::InputException(const char * message) :
	Exception(message)
{
}

InputException::~InputException()
{
}