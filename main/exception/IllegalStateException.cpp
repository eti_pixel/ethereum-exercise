#include "IllegalStateException.h"

using namespace eth;

IllegalStateException::IllegalStateException() :
	Exception("Illegal state")
{
}

IllegalStateException::IllegalStateException(const std::string & message) :
	Exception(message)
{
}

IllegalStateException::IllegalStateException(const char * message) :
	Exception(message)
{
}

IllegalStateException::~IllegalStateException()
{
}