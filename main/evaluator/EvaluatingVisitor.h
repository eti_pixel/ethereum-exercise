#ifndef _EXPRESSION_VISITOR_H_
#define _EXPRESSION_VISITOR_H_

#include <cstdio>
#include "../api/Visitor.h"
#include "../structure/SyntaxTree.h"
#include "../structure/Group.h"
#include "../structure/BinaryOperator.h"
#include "../structure/Literal.h"
#include "../structure/Utility.h"
#include "../exception/InputException.h"
#include "../exception/UnsupportedException.h"

namespace eth
{
	/**
	 * The evaluating visitor first descends into the tree and then incrementaly
	 * evaluates nodes as it moves back towards the root node.
	 */
	class EvaluatingVisitor : public Visitor
	{
	public:
		EvaluatingVisitor();
		virtual ~EvaluatingVisitor();

		virtual ValueType visit(const SyntaxTree & tree) override;
		virtual ValueType visit(const Group & group) override;
		virtual ValueType visit(const BinaryOperator & op) override;
		virtual ValueType visit(const Literal & literal) override;
	};
}

#endif