#ifndef _EVALUATOR_H_
#define _EVALUATOR_H_

#include <memory>
#include "EvaluatingVisitor.h"

namespace eth
{
	class Evaluator
	{
	public:
		Evaluator(std::shared_ptr<SyntaxTree> tree);
		virtual ~Evaluator();

		ValueType evaluate() const;

	protected:
		std::shared_ptr<SyntaxTree> tree;
	};
}

#endif