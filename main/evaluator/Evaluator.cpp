#include "Evaluator.h"

using namespace std;
using namespace eth;

Evaluator::Evaluator(shared_ptr<SyntaxTree> tree) :
	tree(tree)
{
}

Evaluator::~Evaluator()
{
}

ValueType Evaluator::evaluate() const
{
	EvaluatingVisitor visitor;

	return this->tree->accept(visitor);
}