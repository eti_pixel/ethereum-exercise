#include "EvaluatingVisitor.h"

using namespace std;
using namespace eth;

EvaluatingVisitor::EvaluatingVisitor()
{
}

EvaluatingVisitor::~EvaluatingVisitor()
{
}

ValueType EvaluatingVisitor::visit(const SyntaxTree & tree)
{
	ValueType result = 0;
	const vector<shared_ptr<TreeNode>> & children = tree.getChildren();

	Utility::dumpTree(tree);

	if (tree.getChildCount() < 1)
		throw InputException("Missing expression");
	else if (tree.getChildCount() > 1)
		throw InputException("Missing operator");

	for (size_t i = 0; i < children.size(); ++i)
		result += children[i]->accept(*this);

	return result;
}

ValueType EvaluatingVisitor::visit(const Group & group)
{
	if (group.getChildCount() <= 0)
		throw InputException("No expression between parentheses");
	else if (group.getChildCount() > 1)
		throw IllegalStateException("Detected a wrongly parsed group expression");

	return group.getChildren()[0]->accept(*this);
}

ValueType EvaluatingVisitor::visit(const BinaryOperator & op)
{
	ValueType result = 0;
	ValueType childResult;
	const vector<shared_ptr<TreeNode>> children = op.getChildren();
	const char code = op.getCode();

	for (size_t i = 0; i < children.size(); ++i)
	{
		childResult = children[i]->accept(*this);

		if (i == 0)
		{
			result = childResult;
			continue;
		}

		switch (code)
		{
		case BinaryOperator::ADDITION:
			result += childResult;
			break;
		case BinaryOperator::SUBTRACTION:
			result -= childResult;
			break;
		case BinaryOperator::MULTIPLICATION:
			result *= childResult;
			break;
		case BinaryOperator::DIVISION:
			if (childResult == 0)
				throw InputException("Division by zero");
			result /= childResult;
			break;
		default:
			throw UnsupportedException("Unsupported operator");
		}
	}

	return result;
}

ValueType EvaluatingVisitor::visit(const Literal & literal)
{
	return literal.getValue();
}