#include <iostream>
#include <exception>
#include "Controller.h"

using namespace std;
using namespace eth;

int main(int argc, char * argv[])
{
	try
	{
		Controller controller;
		controller.run();
		return 0;
	}
	catch (Exception & e)
	{
		cerr << "An exception has occurred: " << e.what() << endl;
		return 1;
	}
	catch (...)
	{
		cerr << "An unhandled exception has occurred." << endl;
		throw;
		return 1;
	}
}