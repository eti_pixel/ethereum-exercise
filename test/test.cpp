#include "gtest/gtest.h"

#include <iostream>
#include "../main/parser/Parser.h"
#include "../main/evaluator/Evaluator.h"
#include "../main/structure/SyntaxTree.h"

using namespace std;
using namespace eth;

ValueType eval(const string & expression);

// Basics

TEST(Test, testSimpleAddition)		{ EXPECT_EQ(3,	eval("1 + 2")); }
TEST(Test, testSimpleSubtraction)	{ EXPECT_EQ(-1,	eval("1 - 2")); }
TEST(Test, testSimpleMultiplication){ EXPECT_EQ(2,	eval("1 * 2")); }
TEST(Test, testSimpleDivision)		{ EXPECT_EQ(0.5,eval("1 / 2")); }

// Operator precedence

TEST(Test, testOperatorPrecedence1)	{ EXPECT_EQ(11,	eval("1 + 2 * 3 + 4")); }
TEST(Test, testOperatorPrecedence2)	{ EXPECT_EQ(0.5,eval("1 + 2 / 4 - 1")); }
TEST(Test, testOperatorPrecedence3)	{ EXPECT_EQ(1,	eval("1 + 2 * 2 / 4 - 1")); }
TEST(Test, testOperatorPrecedence4)	{ EXPECT_EQ(2,	eval("1 + 2 * 2 * 2 / 4 - 1")); }

// Grouping

TEST(Test, testGrouping1)			{ EXPECT_EQ(3,	eval("(1 + 2)")); }
TEST(Test, testGrouping2)			{ EXPECT_EQ(4,	eval("(4)")); }
TEST(Test, testGrouping3)			{ EXPECT_EQ(-2,	eval("0-1+(0-1)")); }
TEST(Test, testGrouping4)			{ EXPECT_EQ(2,	eval("0-1+((2-3))+((((4))))")); }

// Empty space

TEST(Test, testEmptySpace1)			{ EXPECT_EQ(3,	eval("1  +  2")); }
TEST(Test, testEmptySpace2)			{ EXPECT_EQ(3,	eval("1 +\t2")); }
TEST(Test, testEmptySpace3)			{ EXPECT_EQ(3,	eval("1 +\n2")); }
TEST(Test, testEmptySpace4)			{ EXPECT_EQ(3,	eval("  1 + 2\n")); }

// Division by zero

TEST(Test, testDivisionByZero1)		{ EXPECT_THROW(eval("1 + 2 / 0 + 1"), InputException); }
TEST(Test, testDivisionByZero2)		{ EXPECT_THROW(eval("1 + 2 / (2 / 2 - 1)"), InputException); }

// Invalid input

TEST(Test, testEmptyInput1)			{ EXPECT_THROW(eval(""), InputException); }
TEST(Test, testEmptyInput2)			{ EXPECT_THROW(eval(" "), InputException); }
TEST(Test, testEmptyInput3)			{ EXPECT_THROW(eval("\t"), InputException); }
TEST(Test, testEmptyInput4)			{ EXPECT_THROW(eval("\n"), InputException); }

TEST(Test, testMissingOperand1)		{ EXPECT_THROW(eval("+"), InputException); }
TEST(Test, testMissingOperand2)		{ EXPECT_THROW(eval("1++1"), InputException); }
TEST(Test, testMissingOperand3)		{ EXPECT_THROW(eval("1+"), InputException); }
TEST(Test, testMissingOperand4)		{ EXPECT_THROW(eval("+1"), InputException); }
TEST(Test, testMissingOperand5)		{ EXPECT_THROW(eval("-1"), InputException); }
TEST(Test, testMissingOperand6)		{ EXPECT_THROW(eval("+1+"), InputException); }

TEST(Test, testMissingOperator1)	{ EXPECT_THROW(eval("12"), InputException); }
TEST(Test, testMissingOperator2)	{ EXPECT_THROW(eval("1 2"), InputException); }
TEST(Test, testMissingOperator3)	{ EXPECT_THROW(eval("1 (2)"), InputException); }
TEST(Test, testMissingOperator4)	{ EXPECT_THROW(eval("1 + 2 * 3 (4 - 5)"), InputException); }
TEST(Test, testMissingOperator5)	{ EXPECT_THROW(eval("1 + 2 * 3 + (4 5)"), InputException); }

TEST(Test, testEmptyGroup1)			{ EXPECT_THROW(eval("()"), InputException); }
TEST(Test, testEmptyGroup2)			{ EXPECT_THROW(eval("1 + () + 2"), InputException); }

TEST(Test, testMissingBracket1)		{ EXPECT_THROW(eval("("), InputException); }
TEST(Test, testMissingBracket2)		{ EXPECT_THROW(eval(")"), InputException); }
TEST(Test, testMissingBracket3)		{ EXPECT_THROW(eval("1 + (2 + 3)) + 4"), InputException); }
TEST(Test, testMissingBracket4)		{ EXPECT_THROW(eval("1 + (2) + 3) + 4"), InputException); }
TEST(Test, testMissingBracket5)		{ EXPECT_THROW(eval("1 + ((2 + 3) + 4"), InputException); }
TEST(Test, testMissingBracket6)		{ EXPECT_THROW(eval("1 + (2 + (3) + 4"), InputException); }

int main(int argc, char * argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

ValueType eval(const string & expression)
{
	shared_ptr<SyntaxTree> tree = make_shared<SyntaxTree>();

	Parser(tree).parse(expression);

	return Evaluator(tree).evaluate();
}