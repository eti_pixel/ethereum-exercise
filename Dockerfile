FROM alpine AS runtime

WORKDIR /opt/app

RUN apk add --no-cache libstdc++ libc6-compat && \
	echo 'alias l="ls -alh"' >> /etc/profile && \
	echo 'alias install="apk add --no-cache"' >> /etc/profile

COPY --from=ethereum_exercise-build /opt/app ./

ENV ENV=/etc/profile
ENTRYPOINT bin/evaluate